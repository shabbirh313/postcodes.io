module.exports = {
	nearest: {
		radius: {
			DEFAULT: 1,
			MAX: 250000
		},
		limit: {
			DEFAULT: 100000,
			MAX: 1000000
		}
	},
	search: {
		limit: {
			DEFAULT: 100000,
			MAX: 1000000000
		}
	},
	bulkGeocode: {
		geolocations: {
			MAX: 100
		}
	},
	bulkLookups: {
		postcodes: {
			MAX: 100
		}
	},
	nearestOutcodes: {
		radius: {
			DEFAULT: 5000,
			MAX: 250000
		},
		limit: {
			DEFAULT: 100000,
			MAX: 1000000
		}
	}
};
