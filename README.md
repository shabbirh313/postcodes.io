# Postcode Information System (NodeJS Application)
# Server Provisioning & Installation

## Preamble

Postcodes.io is a lightweight API server to query the UK's [free Postcode Directory](http://www.ons.gov.uk/ons/guide-method/geography/products/postcode-directories/-nspp-/index.html) over HTTP. Postcodes.io currently serves over a million requests every month and has a pretty good uptime record (99.99% at time of writing).

There are scenarios where postcodes.io falls short. Among some of the limitations are: 
- Current limits are too low for expensive queries (such as bulk or range queries)
- You want to avoid the latency of requesting data over the web
- You want firm guarantees about the availability of postcodes.io

The aforementioned shortcomings (among others) can be overcome by hosting (and potentially modifying) your own clone of postcodes.io. Going down this route is actually fairly straightforward and very extremely cheap. 

This wiki provides end-to-end instructions on hosting your own postcodes.io clone on Ubuntu 14.04.

## Installation Steps (<30 mins)

- [Server Provisioning](#provisioning)
- [Install git](#git) (1 min)
- [Install node.js](#nodejs) (1 min)
- [Install Postgres/Postgis](#postgres) (5 mins)
- [Cloning and configuring a local copy of Postcodes.io](#cloning) (1 min)
- [Provisioning your database and downloading postcode data](#database) (5-10 mins)
- [Exposing your server over a network or the web](#expose) (5-10 mins)

###Server Provisioning

For the purposes of this write-up we'll be installing Postcodes.io on an Ubuntu (14.04) VPS over SSH. The VPS requirements are quite low - a single core, 512MB machine would suffice - but don't expect this to be any use in production!!

Before installing postcodes.io, please take a few minutes to secure your VPS and to create a new (non-root) user with sudo privileges. Linode have an excellent guide [here](https://www.linode.com/docs/security/securing-your-server).

### Install git

Git is a version control system. You'll need Git installed to clone postcodes.io

```
sudo apt-get update
```

```
sudo apt-get install git-core
```

### Install node.js

Postcodes.io requires Node.js to run. 

```
sudo apt-get update
```

```
sudo apt-get install python-software-properties python g++ make
```

```
sudo add-apt-repository ppa:chris-lea/node.js
```

```
sudo apt-get update
```

```
sudo apt-get install nodejs
```

### Install Postgresql & Postgis

Postcodes.io underlying datastore is Postgresql. We also need the Postgis extension to allow for geolocation queries.

Please take care copying the instructions below as horizontal scrolling might cause you to cut off the ends.

```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt trusty-pgdg main" >> /etc/apt/sources.list'
```

```
wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -
```

```
sudo apt-get update
```

```
sudo apt-get install postgresql-9.3-postgis-2.1 pgadmin3 postgresql-contrib
```

The initial setup for postcodes.io requires a Postgresql user with "superuser" privileges. The following instructions will allow you to create a new superuser as postgres. Please fill in <your_username> with your  username.

```
sudo su postgres
```

```
createuser <your_username> -s
```

```
exit
```

### Cloning and configuring a local copy of Postcodes.io<

With Git installed, you can download a clone of postcodes.io into your home directory and install dependencies.

```
cd ~
```

```
git clone https://shabbirh313@bitbucket.org/shabbirh313/postcodes.io.git
```

```
cd postcodes.io/ && npm install
```

### Provisioning your database and downloading postcode data

Postcodes.io is packaged with a script which will provision postgres with the necessary user, database and extensions. It will then download a hosted copy of our database and pipe it straight into Postgresql.

```
npm run setup
```

At the start of the setup process, you will be prompted to enter your Postgres user created earlier (i.e. `<your_username>`).

If your user credentials are accepted, the script will proceed to download and load the database. This could take up to 5 minutes.

### Run the App

The following command will run the app on Port 8000 (by default)

```
node server.js
```

You can run a quick test to see it working using curl

```
curl localhost:8000/postcodes/NR305NR
```

When running in production, we strongly advise using a process manager. A process manager will easily allow you to run the server as a background process and will automatically restart the server if it dies. We're fans of [pm2](https://github.com/Unitech/pm2). To install and run postcodes.io with pm2, follow these instructions.

```
sudo npm install pm2 -g --unsafe-perm
```

```
pm2 start server.js
```

You can check on the server with `pm2 list`

You can restart the server with `pm2 restart all`

### Exposing your server over a network or the web

By default, postcodes.io will run on port 8000. As long as port 8000 is not blocked by your firewall, you can immediately access your API with http://your_server_ip_address:8000/

To run on a different port (e.g. 80), you will need to modify the <code>PORT</code> under the "development" configuration object in `config/config.js`. To do that locally,

```
nano config/config.js
```

If you'll be running your postcodes.io clone on a trusted network, it should be fine just to expose your app on whichever port you choose.

If you'll be exposing your postcodes.io clone on the web, we strongly recommend using a webserver like [Nginx](http://nginx.org/) to pass requests to your app as a reverse proxy. I.e. Nginx will listen on port 80 and forward requests to the postcodes.io app on port 8000. Below is a quick and dirty way to configure Nginx to do that.

Install nginx:

```
sudo apt-get update
sudo apt-get install nginx
```

Configure nginx to be a reverse proxy: 

Edit `/etc/nginx/nginx.conf` to use the following bare-bones configuration file. 

Swap `<SERVER_IP>` and `<YOUR_USERNAME>` with the IP address of your server and your unix username respectively.

```
user www-data;
worker_processes 4;
pid /run/nginx.pid;

events {
	worker_connections 768;
}

http {

	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	gzip on;
	gzip_disable "msie6";

	include /etc/nginx/conf.d/*.conf;
	
	upstream api_server {
		server 127.0.0.1:8000;
	}

	server {
		listen 80;
		listen [::]:80;
		server_name <YOUR_SERVER_IP>;

		location / {
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header Host $http_host;
			proxy_set_header X-NginX-Proxy true;
			proxy_pass http://api_server/;
			proxy_redirect off;
		}
		location ~ ^/(images/|css/|js/|favicon.ico) {
			root /home/<YOUR_USERNAME>/postcodes.io/public;
			access_log off;
		}
	}
}


```

And finally restart nginx with `sudo service nginx restart` and make sure your app is already running and listening on port 8000. You should now be able to query your API server over the internet.


#### postcodes.io - forked and modified from https://github.com/ideal-postcodes/postcodes.io